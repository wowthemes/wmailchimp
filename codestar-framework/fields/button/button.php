<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
/**
 *
 * Field: Button
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
class CSFramework_Option_buttonwitheditor extends CSFramework_Options {

  public function __construct( $field, $value = '', $unique = '' ) {
    parent::__construct( $field, $value, $unique );
  }

  public function output(){

  	//wp_enqueue_style('codemirror');

    echo $this->element_before();

     $options    = $this->field['options'];
     $options    = ( is_array( $options ) ) ? $options : array_filter( $this->element_data( $options ) );

    if( isset( $this->field['settings'] ) ) { extract( $this->field['settings'] ); }

    if( !empty( $options ) ){
        foreach ( $options as $key => $value ) {
          echo '<button type="button" name="'. $key .'" class="button">'. $value .'</button>';
        }
    }
    
    echo '<div class="cs-field-textarea"><textarea id="code" name="'. $this->element_name() .'"'. $this->element_class('wmail-textarea') . $this->element_attributes() .'>'. $this->element_value() .'</textarea></div>';

    echo $this->element_after();

  }

}