<?php if( !defined('ABSPATH') ) die('Restricted'); 

$api_key = get_option('_wmailchimp_mail_api_key'); //print_r($api_key); exit; ?>

<h2 class="title"><?php esc_html_e('Mailchimp Settings', 'wmailchimp'); ?></h2>

<div class="_wmailchimp_settings_msg">
	<div class="check_connect">
		<?php if($api_key): 

			$res = wmailchimp_getlist($api_key); 
			
			if($res):?>
				<div class="updated"><p><?php esc_html_e('Connected', 'wmailchimp'); ?></p></div>
			<?php else: ?>
				<div class="error"><p><?php esc_html_e('Not Connected', 'wmailchimp'); ?></p></div>
			<?php endif; ?>
		<?php else: ?>
			<div class="error"><p><?php esc_html_e('Not Connected', 'wmailchimp'); ?></p></div>
		<?php endif; ?>
	</div>
</div>

<table class=form-table>
	<tbody>
		
		<tr>
			<td><label for="blogname"><?php esc_html_e('Mailchimp API key', 'wmailchimp'); ?></label></td>
			<td>
				<input name="blogname" type="text" id="blogname" value="<?php echo esc_attr($api_key); ?>" class="regular-text">
				<input type="button" id="mailchip-connect" class="button button-primary" value="Connect" />
				<span class="spinner" style="float:none;"></span>
				<div class="clearfix"></div>
				<p><small><?php printf( esc_html__('To get the %s API key, go to Mailchimp Account Settings > Extras > API', 'wmailchimp'), '<a href="http://mailchimp.com" target="_blank">Mailchimp</a>'); ?></small></p>
			</td>
		</tr>
	</tbody>
</table>

