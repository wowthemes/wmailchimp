jQuery(document).ready(function($){
	
	$('.wmailchimp_form').on('submit', function(e){
		e.preventDefault();
		var thisis = this;
		var ajaxurl = $(this).attr('action');
		
		var email = $(this).find('input[name=EMAIL]').val();
		//var name = $(this).find('input[name=FULLNAME]').val();
		
		var data = {action: '_wmailchimp_ajax_callback', subaction: 'subscribe'};
		
		//console.log($(this).find('input, button, select, textarea').serializeArray());
		//return;
		$.each( $(this).find('input, button, select, textarea').serializeArray(), function(i, e){
			
			//data[$(this).attr('name')] = $(this).val();
			data[e.name] = e.value;
		} );
		
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var is_valid = regex.test(email);
		
		if( is_valid == true ) {
			
			$(this).find('input, button').attr('disabled', 'disabled');
			$.ajax({
				url: ajaxurl,
				type: 'POST',
				data: data,
				
				success: function( res ) {
					$('._wmailchimp_subscribe_response').html(res);
					
					$(thisis).find('input, button').removeAttr('disabled');
				}
			});
		}
		else {
			alert('Invalid email address, please provide a valid email');
		}
	} );
	
} );