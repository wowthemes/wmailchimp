jQuery(document).ready(function($) {
	$('#mailchip-connect').on('click', function(e) {
		var api = $(this).prev('input').val();
		var thisis = this;

		$(thisis).next('.spinner').addClass('is-active');

		$('._wmailchimp_settings_msg').fadeOut('slow');
		
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {action: '_wmailchimp_ajax_callback', subaction: 'connect', api: api},
			
			success: function(res) {

				if(res.status !== undefined) {

					if( res.status == 'success' ) {
						$('._wmailchimp_settings_msg').html('<div class="updated"><p>Successfully Connected</p></div>');
					}
					else {
						$('._wmailchimp_settings_msg').html('<div class="error"><p>There is an error with connect request</p></div>');
					}
				}
				else {
					$('._wmailchimp_settings_msg').html('<div class="error"><p>There is an error with connect request</p></div>');
				}
				
				$('._wmailchimp_settings_msg').fadeIn('slow');
			},
			complete: function(res) {
				$(thisis).next('.spinner').removeClass('is-active');
			}
		});
	});
});