jQuery(document).ready(function($){

	//var myCodeMirror = CodeMirror(document.body);
    var myMirror = CodeMirror.fromTextArea(document.getElementById("code"), {
      lineNumbers: true,
      mode: "htmlmixed",
      theme: 'xq-light'
    });

    // Show dialogs
     $('.cs-sections button[name=action]').on('click', function(e){
        $('#wmailchimp_action_modal').dialog();
     });
     $('.cs-sections button[name=choice]').on('click', function(e){
        $('#wmailchimp_choice_modal').dialog();
     });
     $('.cs-sections button[name=submit]').on('click', function(e){
        $('#wmailchimp_submit_modal').dialog();
     });
     $('.cs-sections button[name=fields]').on('click', function(e){
        $('#wmailchimp_fields_modal').dialog();
     });

     // Form Fields Modal
     $('#wmailchimp_insert_list_field').on('click', function(e){
        e.preventDefault();

        var $main = $(this).parents('#wmailchimp_fields_modal');
        var $checked = $main.find('input[name=mailchip_field]:checked');
        console.log($checked);
        var $tr = $checked.parents('tr');
        var $list_id = $tr.data('list');
        var $merge_id = $tr.data('merge_id');
        //var $label = $tr.find('input[name=field_label]');
        var $class = $main.find('input[name=field_class]').val();
        var $field_label = $main.find('input[name=field_label]').val();
        var $wrap = $main.find('input[name=wrap]:checked');
        var $show = $main.find('input[name=label]:checked');

        $class = ( $class.length ) ? ' class="' + $class + '" ' : '';
        var $output = '';
        if( $wrap.length ) $output += "<p>\n\t"
        if( $show.length ) $output += '<label>' + $field_label + '</label>' + "\n\t";
        $output += '[wmailchimp_field list="' + $list_id + '" merge_id="'+ $merge_id + '" ' + $class +']' + "\n";
        
        if( $wrap.length ) $output += '</p>'+"\n";

        myMirror.replaceSelection($output);
        $('#wmailchimp_fields_modal').dialog('close');
     });

     // Submit Button Modal
     $('#wmailchimp_insert_submit').on('click', function(e){
        e.preventDefault();

        var $main = $(this).parents('#wmailchimp_submit_modal');
        
        var $submit_value = $main.find('input[name=submit_value]').val();
        var $wrap = $main.find('input[name=wrap]:checked');

        var $output = '';
        if( $wrap.length ) $output += "<p>\n\t";
        
        $output += '<input type="submit" value="'+ $submit_value + '" />' + "\n";
        
        if( $wrap.length ) $output += '</p>'+"\n";

        myMirror.replaceSelection($output);
        $('#wmailchimp_submit_modal').dialog('close');
     });

     // List Choice Modal
     $('#wmailchimp_insert_list_choice').on('click', function(e){
        e.preventDefault();

        var $main = $(this).parents('#wmailchimp_choice_modal');
        var $checked = $main.find('input[name=list_choice]:checked');
        console.log($checked);
        //var $tr = $checked.parents('tr');
        //var $list_id = $tr.data('list');
        //var $merge_id = $tr.data('merge_id');
        //var $label = $tr.find('label');
        var $choice_label = $main.find('input[name=choice_label]').val();
        var $wrap = $main.find('input[name=wrap]:checked');
        var $list_choice = $main.find('input[name=list_choice]:checked');
        //var $list_value = $main.find('input[value]:checked');

        var $output = '';
        if( $wrap.length ) $output += "<p>\n\t";
        if( $choice_label.length ) $output += '<label>' + $choice_label + '</label>' + "\n\t";
        

        $list_choice.each(function(i, e){
        	$output += '<label>\n\t<input name="_wmailchimp_list[]" value="' + $(e).val() + '" type="checkbox" checked /> <span>' + $(e).data('label') + '</span>\n\t</label>' + "\n";
        });
        
        
        if( $wrap.length ) $output += '</p>'+"\n";

        myMirror.replaceSelection($output);
        $('#wmailchimp_choice_modal').dialog('close');
     });

     // Form Action Modal
     $('#wmailchimp_insert_form_action').on('click', function(e){
        e.preventDefault();

        var $main = $(this).parents('#wmailchimp_action_modal');
        var $select = $main.find('select[name=drop]').val();
        console.log($select);
        var $action_selected = $main.find('input[name=action_selected]:checked').val();
        console.log($action_selected);

        var $action_label = $main.find('input[name=action_label]').val();
        var $wrap = $main.find('input[name=wrap]:checked');

        var $output = '';
        if( $wrap.length ) $output += "<p>\n\t"
        if( $action_label.length ) $output += '<label>' + $action_label + '</label>' + "\n\t";

    	if( $select == 'select' ) {
    		if($action_selected == 'subscribe'){
    			$output += '<select>\n\t<option value="subscribe" selected="selected" >Subscribe</option>\n\t';
    			$output += '\t<option value="unsubscribe" >Unsubscribe</option>\n\t</select>' + "\n";
    		}
    		else{
    			$output += '<select>\n\t<option value="subscribe">Subscribe</option>\n\t';
    			$output += '\t<option value="unsubscribe" selected="selected" >Unsubscribe</option>\n\t</select>' + "\n";
    		}
    	}
        else {
        	if($action_selected == 'subscribe'){
	        	$output += '<label>\n\t<input name="_wmailchimp_action" value="subscribe" type="radio" checked /> <span>Subscribe</span>\n\t</label>' + "\n";
	        	$output += '<label>\n\t<input name="_wmailchimp_action" value="unsubscribe" type="radio" /> <span>Unsubscribe</span>\n\t</label>' + "\n";
	        }
	        else{
	        	$output += '<label>\n\t<input name="_wmailchimp_action" value="subscribe" type="radio"  /> <span>Subscribe</span>\n\t</label>' + "\n";
	        	$output += '<label>\n\t<input name="_wmailchimp_action" value="unsubscribe" type="radio" checked /> <span>Unsubscribe</span>\n\t</label>' + "\n";
	        }
        }
        if( $wrap.length ) $output += '</p>'+"\n";

        myMirror.replaceSelection($output);
        $('#wmailchimp_action_modal').dialog('close');
     });

});