<?php
/**
 * Plugin Name:WowThemes Mailchimp
 * Description: This plugin is compatible with all Wowthemes wordpress themes.
 * Author: Shahbaz Ahmed
 * Author URI: http://wow-themes.com
 * Version: 1.0
 * Text Domain: wmailchimp
 *
 * @package  wmailchimp
 */

define( 'WMAILCHIMP_PATH', plugin_dir_path( __FILE__ ) );
define( 'WMAILCHIMP_URL', plugins_url( '', __FILE__ ) );

include_once WMAILCHIMP_PATH . '/ajax.php' ;
require_once WMAILCHIMP_PATH . '/functions.php';

/**
 * [wmailchimp_plugin_load_plugin_textdomain description]
 *
 * @return void [description]
 */
function wmailchimp_plugin_load_plugin_textdomain() {
	load_plugin_textdomain( 'wmailchimp', false, basename( dirname( __FILE__ ) ) . '/languages/' );
}

add_action( 'plugins_loaded', 'wmailchimp_plugin_load_plugin_textdomain' );

add_action( 'admin_menu', 'wmailchimp_register_my_custom_submenu_page' );
add_action( 'admin_enqueue_scripts', 'wpmailchimp_admin_enqueue' );
add_action( 'wp_enqueue_scripts', 'wpmailchimp_wp_enqueue' );

/**
 * [wmailchimp_register_my_custom_submenu_page description]
 *
 * @return void [description]
 */
function wmailchimp_register_my_custom_submenu_page() {

	add_submenu_page(
	    'options-general.php',
	    'Mailchimp Settings',
	    'Mailchimp Settings',
	    'manage_options',
	    'mailchimp-settings',
	    'wmailchimp_settings_submenu'
	);
}


/**
 * [wmailchimp_settings_submenu description]
 *
 * @return void [description]
 */
function wmailchimp_settings_submenu() {
	include 'settings-form.php';
}

/**
 * [wpmailchimp_admin_enqueue description]
 *
 * @return void [description]
 */
function wpmailchimp_admin_enqueue() {
	global $post_type;

	if ( 'mc_form' === $post_type ) {

		wp_enqueue_style( 'wmailchimp_codemirror', WMAILCHIMP_URL.'/assets/css/codemirror.css' );
		$codem_scripts = array( 'codemirror', 'show-hint', 'xml-hint', 'html-hint', 'xml', 'javascript', 'css', 'htmlmixed' );

		foreach ( $codem_scripts as $cods ) {
			wp_enqueue_script( 'codemirror' . $cods, WMAILCHIMP_URL.'/assets/js/codemirror/'.$cods.'.js', '', '', true );
		}
		wp_enqueue_script( 'wmailchimp_form_handler', WMAILCHIMP_URL.'/assets/js/wmailchimp.admin.js', '', '', true );

		if ( ! class_exists( 'CSFramework_Option_buttonwitheditor' ) ) {

			require_once WMAILCHIMP_PATH . 'codestar-framework/fields/button/button.php';
		}
	}

	if ( ! isset( $_GET['page'] ) ) {
		return;
	}

	if ( 'mailchimp-settings' !== $_GET['page'] ) {
		return;
	}

	wp_enqueue_script( 'wmailchimp_settings_handler', WMAILCHIMP_URL.'/assets/js/scripts.js' );

}

/**
 * [wpmailchimp_wp_enqueue description]
 *
 * @return void [description]
 */
function wpmailchimp_wp_enqueue() {

	wp_register_script( 'wmailchimp_front_handler', WMAILCHIMP_URL.'/assets/js/front.js' );
}

add_filter( 'cs_metabox_options', 'wmailchimp_cs_metabox_options' );

/**
 * [wmailchimp_cs_metabox_options description]
 *
 * @param  array $options [description].
 * @return [type]          [description]
 */
function wmailchimp_cs_metabox_options( $options ) {

	$new = require WMAILCHIMP_PATH . '/metabox.config.php';

	return $options;
}


add_action( 'init', 'wmailchimp_mail_forms_init', 500 );


if ( ! function_exists( 'wmailchimp_mail_forms_init' ) ) {
	/**
	 * Register a book post type.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_post_type
	 */
	function wmailchimp_mail_forms_init() {
		$labels = array(
			'name'               => _x( 'Forms', 'post type general name', 'wmailchimp' ),
			'singular_name'      => _x( 'Form', 'post type singular name', 'wmailchimp' ),
			'menu_name'          => _x( 'MC Forms', 'MC Form', 'wmailchimp' ),
			'name_admin_bar'     => _x( 'Form', 'add new on admin bar', 'wmailchimp' ),
			'add_new'            => _x( 'Add New', 'Form', 'wmailchimp' ),
			'add_new_item'       => __( 'Add New Form', 'wmailchimp' ),
			'new_item'           => __( 'New Form', 'wmailchimp' ),
			'edit_item'          => __( 'Edit Form', 'wmailchimp' ),
			'view_item'          => __( 'View Form', 'wmailchimp' ),
			'all_items'          => __( 'All Forms', 'wmailchimp' ),
			'search_items'       => __( 'Search Forms', 'wmailchimp' ),
			'parent_item_colon'  => __( 'Parent Forms:', 'wmailchimp' ),
			'not_found'          => __( 'No Forms found.', 'wmailchimp' ),
			'not_found_in_trash' => __( 'No Forms found in Trash.', 'wmailchimp' ),
		);

		$args = array(
			'labels'             => $labels,
	        'description'        => __( 'Description.', 'wmailchimp' ),
			'public'             => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'mc_form' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => true,
			'supports'           => array( 'title' ),
			'menu_icon'			 => 'dashicons-email',
		);

		register_post_type( 'mc_form', $args );
	}
}


/**
 * [wmailchimp_output_shortcode description]
 *
 * @param array  $atts    [description].
 * @param string $content [<description>].
 * @return [type]       [description]
 */
function wmailchimp_output_shortcode( $atts, $content = null ) {

	$atts = shortcode_atts( array(
		'id' => '',
	), $atts, 'wmailchimp' );
	extract( $atts );

	return include WMAILCHIMP_PATH . '/shortcode_output.php';

}
add_shortcode( 'wmailchimp', 'wmailchimp_output_shortcode' );

/**
 * [wmailchimp_getlist description]
 *
 * @param  [type] $api [description].
 * @return [type]      [description]
 */
function wmailchimp_getlist( $api ) {

	$ssl1 = get_option( '_wmailchimp_mail_api_ssl' );

	$ssl = ( '' !== $ssl1 ) ? $ssl1 : true;

	if ( ! class_exists( 'MailChimp' ) ) {
		include_once WMAILCHIMP_PATH . 'src/MailChimp.php';
	}

	$mailchimp = new MailChimp( $api );

	if ( ! $ssl ) {
		$mailchimp->verify_ssl = false;
	}

	$lists = $mailchimp->get( 'lists' );

	if ( ! $lists ) {

		$mailchimp->verify_ssl = false;
		$lists = $mailchimp->get( 'lists' );

		if ( '' === $ssl1 ) {
			update_option( '_wmailchimp_mail_api_ssl', 'false' );
		}
	}

	update_option( '_wmailchimp_mail_api_key', $api );

	if ( $lists ) {
		update_option( '_wmailchimp_mail_api_lists', $lists );
	}

	if ( $lists ) {
		return true;
	}

	return false;

}

/**
 * [wmailchimp_getfields description]
 *
 * @param  [type] $listid [description].
 * @return [type]         [description]
 */
function wmailchimp_getfields( $listid ) {

	$ssl1 = get_option( '_wmailchimp_mail_api_ssl' );

	$ssl = ( '' !== $ssl1 ) ? false : true;

	$api = get_option( '_wmailchimp_mail_api_key' );

	if ( ! $api ) {
		return false;
	}

	if ( ! class_exists( 'MailChimp' ) ) {
		include_once WMAILCHIMP_PATH . 'src/MailChimp.php';
	}

	$mailchimp = new MailChimp( $api );

	if ( ! $ssl ) {
		$mailchimp->verify_ssl = false;
	}

	$fields = $mailchimp->get( 'lists/'.esc_attr( $listid ).'/merge-fields' );

	if ( $fields ) {
		update_option( '_wmailchimp_mail_api_fields'.$listid, $fields );
	}

	if ( $fields ) {
		return $fields;
	}

	return false;

}

add_action( 'admin_footer', 'wmailchimp_admin_footer' );

/**
 * [wmailchimp_admin_footer description]
 *
 * @return void [description]
 */
function wmailchimp_admin_footer() {

	global $post_type;

	if ( 'mc_form' !== $post_type ) {
		return;
	}
	require WMAILCHIMP_PATH.'/templates/form_action.php';
	require WMAILCHIMP_PATH.'/templates/list_choice.php';
	require WMAILCHIMP_PATH.'/templates/submit.php';
	require WMAILCHIMP_PATH.'/templates/form_fields.php';
}

/**
 * [wmailchimp_field_list description]
 *
 * @param  [type] $atts    [description].
 * @param  string $content [description].
 * @return [type]          [description]
 */
function wmailchimp_field_list( $atts, $content = '' ) {

	$atts = shortcode_atts( array(
		'list' => '',
		'merge_id' => '',
		'class' => '',
	), $atts, 'wmailchimp_field' );

	extract( $atts );

	$found_list = get_option( '_wmailchimp_mail_api_fields'.$list );

	$found_array = array();

	if ( $found_list && is_array( $found_list ) ) {

		foreach ( $found_list['merge_fields'] as $key => $value ) {

			if ( isset( $value['merge_id'] ) && $merge_id == $value['merge_id'] ) {
				$found_array = $value;
				break;
			}
		}
	}

	$build_atts = array();

	$input_type = array( 'text', 'hidden', 'checkbox', 'radio', 'password', 'email', 'number' );

	if ( $found_array && is_array( $found_array ) && wpmailchimp_sh_set( $found_array, 'public' ) ) {

		if ( $value = wpmailchimp_sh_set( $found_array, 'tag' ) ) {
			$build_atts['name'] = $value;
		}

		if ( $value = wpmailchimp_sh_set( $found_array, 'required' ) ) {
			$build_atts['required'] = 'required';
		}
		if ( $value = wpmailchimp_sh_set( $found_array, 'default_value' ) ) {
			$build_atts['value'] = $value;
		}
		if ( $value = wpmailchimp_sh_set( $found_array, 'help_text' ) ) {
			$build_atts['title'] = $value;
		}
	}

	$build_atts['class'] = ( $class ) ? $class : 'form-control';
	$build_atts['id'] = uniqid( 'wmailchimp' );

	switch ( wpmailchimp_sh_set( $found_array, 'type' ) ) {
		case 'text':
		case 'hidden':
		case 'password':
		case 'phone':
		case 'url':
		case 'imageurl':
		case 'date':
		case 'number':
			if ( $value = wpmailchimp_sh_set( $found_array, 'name' ) ) {
				$build_atts['placeholder'] = $value;
			}
			return '<input '. wp_kses_post( wmailchim_build_atts( $build_atts ) ) . ' />';
			break;

		case 'radio':
		case 'chceckbox':
			$build_atts['id'] = uniqid( 'wmailchimp' );
			$options = '<h3>'.wpmailchimp_sh_set( $found_array, 'name' ).'</h3>';
			foreach ( $found_array['options']['choices'] as $key => $value ) {
				$options .= '<input type="'.wpmailchimp_sh_set( $found_array, 'type' ).'" '. wp_kses_post( wmailchim_build_atts( $build_atts ) ) . ' value="'.$value.'" /><label>'.$value.'</label>';
			}
			return $options;
			break;

		case 'dropdown':
			$options = '';
			foreach ( $found_array['options']['choices'] as $key => $value ) {
				$options .= '<option value="'.$value.'">'.$value.'</option>';
			}
			return '<h3>'.wpmailchimp_sh_set( $found_array, 'name' ).'</h3>
					<select '. wp_kses_post( wmailchim_build_atts( $build_atts ) ) . ' >
						'.$options.'
					</select>';
			break;

		default:
			if ( $value = wpmailchimp_sh_set( $found_array, 'name' ) ) {
				$build_atts['placeholder'] = $value;
			}
			return '<input '. wp_kses_post( wmailchim_build_atts( $build_atts ) ) . ' />';
			break;
	}

	return false;
}

add_shortcode( 'wmailchimp_field', 'wmailchimp_field_list' );

/**
 * [wpmailchimp_sh_set description]
 *
 * @param  [type] $array [description].
 * @param  [type] $key   [description].
 * @param  string $def   [description].
 * @return [type]        [description]
 */
function wpmailchimp_sh_set( $array, $key, $def = '' ) {

	if ( is_array( $array ) && isset( $array[ $key ] ) ) {
		return $array[ $key ];
	}

	if ( is_object( $array ) && isset( $array->$key ) ) {
		return $array->$key;
	}

	return $def;
}


/**
 * [wmailchim_build_atts description]
 *
 * @param  [type] $atts [description].
 * @return [type]       [description]
 */
function wmailchim_build_atts( $atts ) {
	$string = '';

	foreach ( $atts as $key => $value ) {
		$string .= $key . '="' . $value . '" ';
	}

	return $string;
}

