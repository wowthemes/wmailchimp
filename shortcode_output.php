<?php 

if ( ! defined( 'ABSPATH' ) ) {

	die( 'Restricted' );

}

if ( ! is_numeric( $id ) ) {

	return;

}

wp_enqueue_script( array( 'wmailchimp_front_handler' ) );

ob_start();

$form = get_post( $id );

if ( ! $form ) {

	return;

}

if ( 'mc_form' !== $form->post_type ) {

	return;

}

$meta = get_post_meta( $form->ID, '_form_fields_options', true );

$editor_content = wpmailchimp_sh_set( $meta, 'button_editor' );
$uniqid = uniqid();
$form_id = base64_encode( $form->ID );

?>

<form class="wmailchimp_form" id="<?php echo esc_attr( $uniqid ); ?>" action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>">

	<div class="_wmailchimp_subscribe_response"></div>
	
	<?php echo balanceTags( do_shortcode( $editor_content ) ); ?>
	
	<input type="hidden" name="_mmcform" value="<?php echo esc_attr( $form_id ); ?>" />
	
	<!-- some inputs here ... -->
	<?php wp_nonce_field( 'wmailchimp_subscribe_nonce', 'wmailchimp_subscribe_nonce_field' ); ?>

</form>


<?php return ob_get_clean();

