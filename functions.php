<?php 


function wmailchimp_get_mail_lists()
{
	$lists = get_option('_wmailchimp_mail_api_lists');
	
	//print_r($lists);exit;
	
	$return = array();
	
	if( isset( $lists['lists'] ) ) {
		$new_list = $lists['lists'];
		
		foreach( $new_list as $list ) {
			$return[$list['id']] = $list['name']; 
		}
	}
	
	return $return;
}

function wmailchimp_strip_single_tag($str,$tag){

    $str1=preg_replace('/<\/'.$tag.'>/i', '', $str);

    if($str1 != $str){

        $str=preg_replace('/<'.$tag.'[^>]*>/i', '', $str1);
    }

    return $str;
}