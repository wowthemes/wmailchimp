<?php $lists = wmailchimp_get_mail_lists(); //print_r($lists);exit(); ?>

<div id="wmailchimp_choice_modal" style="display:none;">
	<h3><?php esc_html_e( 'LIST CHOICE', 'wmailchimp' ); ?></h3>
	<p><?php esc_html_e( 'This field will allow your visitors to choose a list to subscribe to.', 'wmailchimp' ); ?></p>
	<hr/>
	<div>
		<label style="font-weight:bold;"><?php esc_html_e( 'Field Label: ', 'wmailchimp' ); ?></label>
		<input placeholder="List choice" type="text" name="choice_label">
	</div>
	<hr/>
	<div>
		<label style="font-weight:bold;"><?php esc_html_e( 'List Choices:', 'wmailchimp' ); ?></label>
		<div>
			<table>
				<?php foreach($lists as $key => $value): ?>
					<tr>
						<td><input name="list_choice" value="<?php echo esc_attr($key); ?>" type="checkbox" data-label="<?php echo esc_attr($value); ?>"></td>
						<td><label><?php echo esc_attr($value); ?></label></td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
	<hr/>
	<div>
		<label>
			<input name="wrap" type="checkbox"><?php esc_html_e( 'Wrap in paragraph tags?', 'wmailchimp' ); ?>
		</label>
	</div>
	<p><button class="button-primary" type="button" id="wmailchimp_insert_list_choice"><?php esc_html_e( 'Add to form', 'wmailchimp' ); ?></button></p>
</div>