<div id="wmailchimp_action_modal" style="display:none;">
	<h3><?php esc_html_e( 'FORM ACTION', 'wmailchimp' ); ?></h3>
	<p><?php esc_html_e( 'This field will allow your visitors to choose whether they would like to subscribe or unsubscribe', 'wmailchimp' ); ?></p>
	<hr/>
	<div>
		<label style="font-weight:bold;"><?php esc_html_e( 'Field Label: ', 'wmailchimp' ); ?></label>
		<input placeholder="Form action" type="text" name="action_label">
	</div>
	<hr/>
	<div>
		<label style="font-weight:bold;"><?php esc_html_e( 'Choice Type: ', 'wmailchimp' ); ?></label>
		<select name="drop">
			<option value="select"><?php esc_html_e( 'Dropdown', 'wmailchimp' ); ?></option>
			<option value="radio"><?php esc_html_e( 'Radio buttons', 'wmailchimp' ); ?></option>
		</select>
	</div>
	<div>
		<label style="font-weight:bold;"><?php esc_html_e( 'Form Choices:', 'wmailchimp' ); ?></label>
		<div>
			<table>
				<tr>
					<td><input name="action_selected" value="subscribe" type="radio"></td>
					<td><label><?php esc_html_e( 'Subscribe', 'wmailchimp' ); ?></label></td>
				</tr>
				<tr>
					<td><input name="action_selected" value="unsubscribe" type="radio"></td>
					<td><label><?php esc_html_e( 'Unsubscribe', 'wmailchimp' ); ?></label></td>
				</tr>
			</table>
		</div>
	</div>
	<hr/>
	<div>
		<label>
			<input name="wrap" type="checkbox"><?php esc_html_e( 'Wrap in paragraph tags?', 'wmailchimp' ); ?>
		</label>
	</div>
	
	<p><button class="button-primary" type="button" id="wmailchimp_insert_form_action"><?php esc_html_e( 'Add to form', 'wmailchimp' ); ?></button></p>
</div>