<?php $lists = get_option('_wmailchimp_mail_api_lists'); //print_r($lists);exit;

$fields = array();

if(is_array($lists)){
		$new_list = $lists['lists'];
		
		foreach( $new_list as $list ) {
				
			$return = $list['id']; //print_r($return);exit;
			$fields[ $return ] = wmailchimp_getfields($return);
			$fields[ $return ]['list_name'] = $list['name'];
		}
}

$fields_html = '';
	
	if( $fields ) {
		foreach ( $fields as $k => $fieldlist ) {

			$fields_html .= '
							<tr>
								<th>'.$fieldlist['list_name'].'</th>
							</tr>';
			foreach( $fieldlist['merge_fields'] as $merge_f ) : //print_r($merge_f);exit;
				
				$fields_html .= '<tr data-list="'.$k.'" data-merge_id="'.$merge_f['merge_id'].'">
									<td><input name="mailchip_field" value="1" type="radio" ></td>
									<td><label>'.$merge_f['name'].'</label></td>
								</tr>';
			endforeach; 
		}
	}
?>

<div id="wmailchimp_fields_modal" style="display:none;">
	<h3><?php esc_html_e( 'FORM FIELDS', 'wmailchimp' ); ?></h3>
	<p><?php esc_html_e( 'This field will allow your visitors to choose fields of the form.', 'wmailchimp' ); ?></p>
	<hr/>
	<div>
		<label style="font-weight:bold;"><?php esc_html_e( 'Field Label: ', 'wmailchimp' ); ?></label>
		<input placeholder="Form action" type="text" name="field_label">
	</div>
	<hr/>

	<div>
		
		<div>
			<table>
				<?php echo $fields_html; ?>
				
			</table>
		</div>
	</div>
	<hr/>
	<div>
		<label>
			<input name="wrap" type="checkbox"><?php esc_html_e( 'Wrap in paragraph tags?', 'wmailchimp' ); ?>
		</label>
	</div>
	<div>
		<label>
			<input name="label" type="checkbox"><?php esc_html_e( 'Show label?', 'wmailchimp' ); ?>
		</label>
	</div>
	<hr/>
	<div>
		<label style="font-weight:bold;"><?php esc_html_e( 'Extra Class: ', 'wmailchimp' ); ?></label>
		<input placeholder="Insert Class" name="field_class" type="text">
	</div>
	
	<p><button class="button-primary" type="button" id="wmailchimp_insert_list_field"><?php esc_html_e( 'Add to form', 'wmailchimp' ); ?></button></p>
</div>