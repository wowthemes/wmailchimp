<div id="wmailchimp_submit_modal" style="display:none;">
	<h3><?php esc_html_e('SUBMIT BUTTON', 'wmailchimp' ); ?></h3>
	<div>
		<label style="font-weight:bold;"><?php esc_html_e('Button Label: ', 'wmailchimp' ); ?></label>
		<input type="text" name="submit_value"><p><?php esc_html_e('* Text to prefill value field with.', 'wmailchimp' ); ?></p>
	</div>
	<hr/>
	<div>
		<label>
			<input name="wrap" type="checkbox"><?php esc_html_e('Wrap in paragraph tags?', 'wmailchimp' ); ?>
		</label>
	</div>
	<p><button class="button-primary" type="button" id="wmailchimp_insert_submit"><?php esc_html_e('Add to form', 'wmailchimp' ); ?></button></p>
</div>