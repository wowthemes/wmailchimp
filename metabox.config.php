<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.

global $post;

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// METABOX OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
//$options      = array();

// -----------------------------------------
// Page Side Metabox Options               -
// -----------------------------------------
$options[]    = array(
  'id'        => '_custom_mailchip_side_options',
  'title'     => 'Mailchip Settings',
  'post_type' => 'mc_form',
  'context'   => 'side',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'section_3',
      'fields' => array(
		
		
		array(
		  'type'    => 'notice',
		  'class'   => 'info',
		  'content' => 'Info: Only give the specified names to field "EMAIL", "FULLNAME".',
		),
        array(
          'id'        => 'mailchimp_list',
          'type'      => 'select',
          'options'   => wmailchimp_get_mail_lists(),
          'default'   => '',
		  'attributes' => array( 'class' => 'chosen' ),
		  'help' 		=> 'To update the subscribers list Please go to Settings > Mailchimp Settings and click connect'
        ),
		
		array(
		  'type'    => 'notice',
		  'class'   => 'info',
		  'content' =>  isset($_GET['post']) ? '[wmailchimp id="'.$_GET['post'].'"]' : '[wmailchimp id="YOUR POST ID"]',
		),
		

      ),
    ),

  ),
);



// -----------------------------------------
// Form Fields Options                    -
// -----------------------------------------
$options[]    = array(
  'id'        => '_form_fields_options',
  'title'     => 'Form Fields Options',
  'post_type' => 'mc_form',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    // begin: a section
    array(
      'name'  => 'wp_page_section',
      
      'icon'  => 'fa fa-cog',
      // begin: fields
      'fields' => array(
        
        // begin: a field
        array(
          'type'  => 'subheading',
          'content' => esc_html__( 'Choose a MailChimp field to add to the form', 'wmailchimp' )
        ),
        // End: a field

        array(
          'id'    => 'button_editor',
          'type'  => 'buttonwitheditor',
          'title' => esc_html__( 'Add New Field', 'wmailchimp' ),
          'desc' => '',
          'options' => array('action' => 'Form Action', 'choice' => 'List Choice', 'submit' => 'Submit', 'fields' => 'Form Fields'),
          'default' => ''
        ),
        array(
          'type'    => 'notice',
          'class'   => 'info-editor',
          'content' => esc_html__( 'Info: Only give the specified names to field "EMAIL", "FULLNAME".', 'wmailchimp' )
        ),
    
      ), // end: fields
    ), // end: a section

    

  ),
);


// -----------------------------------------
// Messages Fields Options                    -
// -----------------------------------------
$options[]    = array(
  'id'        => '_message_fields_options',
  'title'     => 'Messages Fields Options',
  'post_type' => 'mc_form',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    // begin: a section
    array(
      'name'  => 'wp_page_section',
      
      'icon'  => 'fa fa-cog',
      // begin: fields
      'fields' => array(
        
        // begin: a field
        array(
          'type'  => 'subheading',
          'content' => esc_html__( 'Form Messages', 'wmailchimp' )
        ),
        // End: a field

        array(
          'id'    => 'subscribed',
          'type'  => 'text',
          'title' => esc_html__( 'Successfully subscribed', 'wmailchimp' ),
          'desc' => esc_html__( 'The text that shows when an email address is successfully subscribed to the selected list(s).', 'wmailchimp' ),
          'default' => esc_html__( 'Thank you, your sign-up request was successful! Please check your email inbox to confirm.', 'wmailchimp' )
        ),

        array(
          'id'    => 'invalid_email',
          'type'  => 'text',
          'title' => esc_html__( 'Invalid email address', 'wmailchimp' ),
          'desc' => esc_html__( 'The text that shows when an invalid email address is given.', 'wmailchimp' ),
          'default' => esc_html__( 'Please provide a valid email address.', 'wmailchimp' )
        ),

        array(
          'id'    => 'required_field_missing',
          'type'  => 'text',
          'title' => esc_html__( 'Required field missing', 'wmailchimp' ),
          'desc' => esc_html__( 'The text that shows when a required field for the selected list(s) is missing.', 'wmailchimp' ),
          'default' => esc_html__( 'Please fill in the required fields.', 'wmailchimp' )
        ),

        array(
          'id'    => 'already_subscribed',
          'type'  => 'text',
          'title' => esc_html__( 'Already subscribed', 'wmailchimp' ),
          'desc' => esc_html__( 'The text that shows when the given email is already subscribed to the selected list(s).', 'wmailchimp' ),
          'default' => esc_html__( 'Given email address is already subscribed, thank you!', 'wmailchimp' )
        ),

        array(
          'id'    => 'error',
          'type'  => 'text',
          'title' => esc_html__( 'General error', 'wmailchimp' ),
          'desc' => esc_html__( 'The text that shows when a general error occured.', 'wmailchimp' ),
          'default' => esc_html__( 'Oops. Something went wrong. Please try again later.', 'wmailchimp' )
        ),

        array(
          'id'    => 'unsubscribed',
          'type'  => 'text',
          'title' => esc_html__( 'Unsubscribed', 'wmailchimp' ),
          'desc' => esc_html__( 'When using the unsubscribe method, this is the text that shows when the given email address is successfully unsubscribed from the selected list(s).', 'wmailchimp' ),
          'default' => esc_html__( 'You were successfully unsubscribed.', 'wmailchimp' )
        ),

        array(
          'id'    => 'not_subscribed',
          'type'  => 'text',
          'title' => esc_html__( 'Not subscribed', 'wmailchimp' ),
          'desc' => esc_html__( 'When using the unsubscribe method, this is the text that shows when the given email address is not on the selected list(s).', 'wmailchimp' ),
          'default' => esc_html__( 'Given email address is not subscribed.', 'wmailchimp' )
        ),

        array(
          'id'    => 'no_lists_selected',
          'type'  => 'text',
          'title' => esc_html__( 'No list selected', 'wmailchimp' ),
          'desc' => esc_html__( 'When offering a list choice, this is the text that shows when no lists were selected.', 'wmailchimp' ),
          'default' => esc_html__( 'Please select at least one list.', 'wmailchimp' )
        ),

        array(
          'id'    => 'previously_unsubscribed',
          'type'  => 'text',
          'title' => esc_html__( 'Previously unsubscribed', 'wmailchimp' ),
          'desc' => esc_html__( 'When someone previously unsubscribed, the plugin can not resubscribe them automatically. Refer to your MailChimp hosted sign-up form here.', 'wmailchimp' ),
          'default' => esc_html__( 'It seems that you have previously unsubscribed, so we can not automatically resubscribe you.', 'wmailchimp' )
        ),
    
      ), // end: fields
    ), // end: a section

    

  ),
);

return $options;

CSFramework_Metabox::instance( $options );
