<?php 

if( !class_exists('WMailChimp_Ajax') )
{
	Class WMailChimp_Ajax
	{
		
		function __construct()
		{
			add_action('wp_ajax__wmailchimp_ajax_callback', array($this, 'ajax') );
		}
		
		function ajax()
		{
			
			$subaction = isset( $_REQUEST['subaction'] ) ? $_REQUEST['subaction'] : '';
			
			if( method_exists($this, $subaction) ) {
				
				$this->$subaction();
			} 
			else {
				echo json_encode( array('status' => 'error', 'msg' => __('No method found ', 'wmailchimp').$subaction ) );
			}
			exit;
		}
		
		function connect()
		{
			
			$api = isset( $_POST['api'] ) ? $_POST['api'] : '';
			
			$api_res = wmailchimp_getlist($api);
			
			header('Content-type: application/json');

			if( !$api_res ) exit(json_encode( array('status' => 'error', 'msg' => __('Failed to retrieve the list', 'wmailchimp') ) ));
			echo json_encode( array('status' => 'success', 'lists' => $lists ) );
			
 		}
		
		function subscribe()
		{
			if ( 
				! isset( $_POST['wmailchimp_subscribe_nonce_field'] ) 
				|| ! wp_verify_nonce( $_POST['wmailchimp_subscribe_nonce_field'], 'wmailchimp_subscribe_nonce' ) 
			) {

			   print esc_html__('Sorry, your nonce did not verify.', 'wmailchimp');
			   exit;

			}
			
			$form_id = base64_decode( $_POST['_mmcform'] );
			
			if( !$form_id ) exit( esc_html__('Invalid Form submitted', 'wmailchimp') );
			
			$listinfo = wpmailchimp_sh_set( get_post_meta( $form_id, '_custom_mailchip_side_options', true ), 'mailchimp_list' );

			$posted_list = (array)wpmailchimp_sh_set($_POST, '_wmailchimp_list');
			$posted_list = array_filter( $posted_list );

			if($listinfo && ! $posted_list) {
				array_push($posted_list, $listinfo);
			}
			
			if($posted_list) {
				foreach ($posted_list as $value) {
					$list_fields = get_option( '_wmailchimp_mail_api_fields'. $value );

					if( $list_fields ) {
						print_r( $this->do_mailchimp_form( $list_fields ) );
					}
					else {
						print_r( esc_html__( 'No List found.', 'wmailchimp' ) );
					}
				}
				exit;
			}
			exit( esc_html__( 'There is an error with the request.', 'wmailchimp' ) );
			
		}


		function do_mailchimp_form( $fields ) {

			$subscribe = wpmailchimp_sh_set( $_POST, '_wmailchimp_action' );

			$subscribe = ( $subscribe == 'unsubscribe' ) ? 'unsubscribe' : 'subscribe';

			$def = array();
			$list_id = '';

			foreach ($fields['merge_fields'] as $key => $value) {
				$tag = wpmailchimp_sh_set( $value, 'tag' );
				//print_r($value);exit;
				if( !$tag ) continue;

				$def[ $tag ] = wpmailchimp_sh_set( $value, 'default_value' );
				$list_id = wpmailchimp_sh_set( $value, 'list_id' );
			}

			$final = array_intersect_key($_POST, $def);

			if( $subscribe == 'unsubscribe' ) {
				return $this->do_unsubscribe( $final, $list_id );
			}
			else {
				return $this->do_subscribe( $final, $list_id );
			}
		}


		function do_unsubscribe( $fields, $list_id ) {

			$api = get_option( '_wmailchimp_mail_api_key' );

			$email = wpmailchimp_sh_set( $_POST, 'EMAIL' );

			if( !class_exists('MailChimp') ) include_once 'src/Mailchimp.php';

			$MailChimp = new MailChimp($api);
			$MailChimp->verify_ssl = false;

			$result = $MailChimp->post("lists/$list_id/members", [
                'email_address' => $email,
                'status'        => 'unsubscribed',
            ]);
			return $this->parse_mc_result( $result, $list_id );
		}


		function do_subscribe( $fields, $list_id ) {

			

			$api = get_option( '_wmailchimp_mail_api_key' );

			$email = wpmailchimp_sh_set( $_POST, 'EMAIL' );

			if( !class_exists('MailChimp') ) {
				include_once WMAILCHIMP_PATH . 'src/MailChimp.php';
			}

			$MailChimp = new MailChimp($api);
			$MailChimp->verify_ssl = false;

			$options = array('email_address' => $email, 'status' => 'subscribed' );
			if( $fields ) $options['merge_fields'] = $fields;

			$result = $MailChimp->post( "lists/$list_id/members", $options );
			//print_r($fields);exit('sdfds');
            return $this->parse_mc_result( $result, $list_id );
		}

		function parse_mc_result( $result, $list_id ) {

			$status = wpmailchimp_sh_set( $result, 'status' );

			$lists = get_option('_wmailchimp_mail_api_lists'); //print_r($lists);exit;
			$list_name = '';

			if(is_array($lists)){				
				foreach( $lists['lists'] as $list ) {
					if( $list['id'] == $list_id ) {
						$list_name = $list['name'];
						break;
					}
				}
			}

			switch ($status) {
				case '400':
					
					$message = '<div class="alert-danger">
									<h5> '. wpmailchimp_sh_set( $result, 'title' ) .'( '.$list_name.' )</h5>
									<p>'.wpmailchimp_sh_set( $result, 'detail' ).'</p>';

					if( $errors = wpmailchimp_sh_set( $result, 'errors' ) ) {
						foreach ($errors as $key => $value) {
							$message .= '<p><span>'.wpmailchimp_sh_set( $value, 'field' ).'</span>'.wpmailchimp_sh_set( $value, 'message' ) . '</p>';
						}
					}

					$message .= '</div>';
					return $message;

					break;
				
				case 'subscribed':
						return '<div class="alert-success">
									<h5> '.$list_name.'</h5>
									<p>'. esc_html__( 'Successfully subscribed', 'wmailchimp' ).'</p></div>';
					break;
				case 'unsubscribed':
					return '<div class="alert-success">
									<h5> '.$list_name.'</h5>
									<p>'. esc_html__( 'Successfully unsubscribed', 'wmailchimp' ).'</p></div>';
					break;
				default:
					return '<div class="alert-danger">'. esc_html__( 'Error with the request, try again later', 'wmailchimp' ).'</div>';;
					break;
			}
		}
	}
}


new WMailChimp_Ajax;